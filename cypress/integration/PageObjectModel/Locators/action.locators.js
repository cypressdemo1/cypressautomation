var ActionsLocator = {

    HeadersLinks: function () {
        return 'ul.navbar-nav li a'
    },

    UserNameInputBox: function () {
        return 'input[placeholder="Username"]'
    },

    UserEmailInputBox: function () {
        return 'input[placeholder="Email"]'
    },
    PasswordInputBox: function () {
        return 'input[placeholder="Password"]'
    },
    ErrorMessage: function () {
        return '.error-messages > :nth-child(1)'
    },

    SignInLink: function () {
        return ':nth-child(2) > .nav-link'
    },

    SignUpButton: function () {
        return 'button[type="submit"]'
    },

    SignInButton: function () {
        return '.btn'
    },

    UserNameLink: function () {
        return 'ul li:nth-child(4)'
    },

    FavIcon: function () {
        return 'div:nth-child(1)  > div > div.pull-xs-right > button > i'
    },

    FavTab: function () {
        return 'a[href*="favorites"]'
    },

    NewPost: function () {
        return 'li.nav-item i.ion-compose'
    },

    ArticleTitleInputField: function () {
        return '[placeholder="Article Title"]'
    },

    ArticleAboutInputField: function () {
        return 'input[placeholder*="article about?"]'
    },

    ArticleDetailsTextArea: function () {
        return 'textarea[placeholder*="Write your article"]'
    },

    Tags: function () {
        return 'input[placeholder*="Enter tags"]'
    },

    ArticlePublishButton: function () {
        return 'button[type="button"]'
    },

    PostCommentButton: function () {
        return '.card-footer > .btn';
    },

    Setting: function () {
        return ':nth-child(3) > .nav-link';
    },

    Logout: function () {
        return 'button[class*="btn-outline-danger"]'
    },

    ProfilePictureUrl:function(){
      return 'input[placeholder="URL of profile picture"]'
    },

    BioInformation:function(){
        return 'textarea[placeholder="Short bio about you"]'
    },

    NewPassword:function(){
        return 'input[placeholder="New Password"]'
    },

    UpdateSettingButton:function(){
        return 'button[class*="btn-primary"]'
    }
};

export default ActionsLocator