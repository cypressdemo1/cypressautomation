import ActionsLocator from "../Locators/action.locators";

var UpdateProfileMethod = {

    EnterProfilePictureUrl: function (ProfilePictureUrl) {
        cy.get(ActionsLocator.ProfilePictureUrl()).clear().type(ProfilePictureUrl);

    },
    EnterBioInformation: function (BioInformation) {
        cy.get(ActionsLocator.BioInformation()).clear().type(BioInformation);

    },

    EnterNewPassword: function (NewPassword) {
        cy.get(ActionsLocator.NewPassword()).clear().type(NewPassword);

    },

    ClickOnUpdateSettingButton:function()
    {
        cy.get(ActionsLocator.UpdateSettingButton()).should('be.visible').contains('Update Settings', { timeout: 1000 }).click()
    },

    ClickOnUpdateSettingButtonFailedTest:function()
    {
        cy.get(ActionsLocator.UpdateSettingButton()).should('be.hidden').contains('Update Settings', { timeout: 1000 }).click()
    }
    
};
export default UpdateProfileMethod