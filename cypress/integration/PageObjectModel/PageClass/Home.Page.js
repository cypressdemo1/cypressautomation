import ActionsLocator from "../Locators/action.locators";

var HomeMethod = {

    clickOnNewPost: function () {
        cy.get(ActionsLocator.NewPost()).should('be.visible').click({ timeout: 10000 })
    },

    clickOnSetting: function () {
        cy.wait(2000)
        cy.get(ActionsLocator.Setting()).should('be.visible').click({ timeout: 50000 })
    },

    clickOnLogout: function () {
        cy.wait(1000)
        cy.get(ActionsLocator.Logout()).should('be.visible').click()
    },

    EnterArticleTitle: function (articleTitle) {
        cy.get(ActionsLocator.ArticleTitleInputField()).type(articleTitle)
    },

    EnterArticleAbout: function (articleAbout) {
        cy.get(ActionsLocator.ArticleAboutInputField()).type(articleAbout)
    },

    EnterArticleDetails: function (articleDetails) {
        cy.get(ActionsLocator.ArticleDetailsTextArea()).type(articleDetails)
    },

    EnterArticleTags: function (articleTags) {
        cy.get(ActionsLocator.Tags()).type(articleTags);
    },

    ClickOnArticlePublishButton: function () {
        cy.get(ActionsLocator.ArticlePublishButton()).click();
        cy.get(ActionsLocator.PostCommentButton()).should('be.visible', { timeout: 1000 });
    },

    clickOnUserName: function () {
        cy.get(ActionsLocator.UserNameLink()).should('be.visible').click();
    },
    clickOnFavIcon: function () {
        cy.wait(500);
        cy.get(ActionsLocator.FavIcon()).should('be.visible').dblclick();
        cy.wait(500);
    },

    clickonFavTab: function () {
        cy.get(ActionsLocator.FavTab()).should('be.visible', { timeout: 10000 }).click();
        cy.wait(500);
    },

    validateFav: function () {
        cy.get('button.btn-primary').should('be.visible').first().then(($fav) => {
            const favCount = $fav.text()
            // expect(parseInt(favCount)).to.eq(1);
            cy.log(favCount)
        });
    }
};
export default HomeMethod