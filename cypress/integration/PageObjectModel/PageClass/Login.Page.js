import ActionsLocator from "../Locators/action.locators";

var LoginMethod = {

    clickOnSignUPLink: function () {

        //Click on SignUp via eq(index)
        //cy.get('ul.navbar-nav li').eq(2).click().end()

        //Click on SignUp Via Each Loop
        cy.get(ActionsLocator.HeadersLinks()).each(($el, index, $list) => {
            cy.log($el.text())
            if ($el.text() === 'Sign up') {
                cy.log($el.text())
                cy.get(ActionsLocator.HeadersLinks()).eq(2).click()
            }
        })
    },
    
    EnterUserName: function (userName) {
        cy.wait(1000)
        cy.get(ActionsLocator.UserNameInputBox()).as('UsernameInputField')
        cy.get('@UsernameInputField').clear().type(userName).blur().focus()

    },

    EnterEmail: function (email) {
        cy.get(ActionsLocator.UserEmailInputBox()).clear().type(email)
    },

    EnterPassword: function (password) {
        cy.get(ActionsLocator.PasswordInputBox()).clear().type(password)
    },

    ClickOnSignUpButton: function () {
        cy.get(ActionsLocator.SignUpButton()).should('be.visible')
            .contains('Sign in').click({ timeout: 1000000 })
    },

    getErrorMessage: function () {
        cy.wait(1000);
        cy.get(ActionsLocator.ErrorMessage()).should('be.visible', { timeout: 100000 });
    },

    ClickOnSignInLink: function () {
        cy.get(ActionsLocator.SignInLink()).should('be.visible')
            .contains('Sign in', { timeout: 1000000 }).click()
    },

    clickonSignInButton: function () {
        cy.get(ActionsLocator.SignInButton()).should('be.visible')
            .contains('Sign in', { timeout: 1000 }).click()
    }
};
export default LoginMethod