import LoginMethod from "../PageClass/Login.Page";
import HomeMethod from "../PageClass/Home.Page";
import UpdateProfileMethod from "../PageClass/UpdateProfile.Page";

describe('Page Object Model', function () {
    beforeEach('Load Url', function () {
        cy.fixture('TestData').then(function (data) {
            this.data = data;
        });
        cy.visit(Cypress.env('QA'))
        cy.Signin()
    });

    it('Signup Functionality Test!', function () {
        cy.Logout();
        LoginMethod.clickOnSignUPLink()
        LoginMethod.EnterUserName(this.data.Username)
        LoginMethod.EnterEmail(this.data.Email)
        LoginMethod.EnterPassword(this.data.Password)
        LoginMethod.ClickOnSignUpButton()
        LoginMethod.getErrorMessage()
        LoginMethod.ClickOnSignInLink()
        LoginMethod.EnterEmail(this.data.Email)
        LoginMethod.EnterPassword(this.data.Password)
        LoginMethod.clickonSignInButton()
        HomeMethod.clickOnNewPost()
    })

    it('Create Article and Make it Fav Test!', function () {
        HomeMethod.clickOnNewPost()
        HomeMethod.EnterArticleTitle(this.data.ArticleTitle)
        HomeMethod.EnterArticleAbout(this.data.ArticleAbout)
        HomeMethod.EnterArticleDetails(this.data.ArticleDescription)
        HomeMethod.EnterArticleTags(this.data.ArticleTag)
        HomeMethod.ClickOnArticlePublishButton()
        HomeMethod.clickOnUserName()
        HomeMethod.clickOnFavIcon()
        HomeMethod.clickonFavTab()
        HomeMethod.validateFav()
    })

    it('Update Setting Test!', function () {
        HomeMethod.clickOnSetting()
        UpdateProfileMethod.EnterProfilePictureUrl(this.data.ProfilePictureUrl)
        UpdateProfileMethod.EnterBioInformation(this.data.BoiInformation)
        UpdateProfileMethod.EnterNewPassword(this.data.NewPassword)
        LoginMethod.EnterUserName(this.data.Username)
        LoginMethod.EnterEmail(this.data.Email)
        UpdateProfileMethod.ClickOnUpdateSettingButton();
    })

    it('Failed Test Case!', function () {
        HomeMethod.clickOnSetting()
        UpdateProfileMethod.EnterProfilePictureUrl(this.data.ProfilePictureUrl)
        UpdateProfileMethod.EnterBioInformation(this.data.BoiInformation)
        UpdateProfileMethod.EnterNewPassword(this.data.NewPassword)
        LoginMethod.EnterUserName(this.data.Username)
        LoginMethod.EnterEmail(this.data.Email)
        UpdateProfileMethod.ClickOnUpdateSettingButtonFailedTest();
    })

    afterEach('Logout From Application!', function () {
        cy.Logout()
    });
})
