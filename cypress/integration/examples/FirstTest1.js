/// <reference types="cypress"/>
before(function(){
    cy.Signin()
    cy.screenshot()
   })

describe('Create first Post',function(){
   it('Secound Test case',function(){
       cy.screenshot()
     cy.get('[placeholder="Article Title"]').type('First Article about CypressTesting!');
     cy.get('input[placeholder*="article about?"]').type('css css-selectors conditional-operator');
     cy.get('textarea[placeholder*="Write your article"]').type('css css-selectors conditional-operator');
     cy.get('input[placeholder*="Enter tags"]').type('ALOK SINGH');
     cy.get('input[placeholder*="Enter tags"]').type('ALOK SINGH');
     cy.get('button[type="button"]').should('be.visible').contains('Publish Article',{timeout:1000000}).click();
     cy.url().should('include','article');
     cy.get('h1').should('be.visible').contains('First Article about CypressTesting!',{timeout:1000000});
     cy.get('a.nav-link').should('be.visible').contains('Home',{timeout:1000}).click();
     cy.get('li.nav-item a.active').should('be.visible').contains('Your Feed',{timeout:100000}).click();
   })
})

describe('Make article Fav',function(){
   it('Third Test case',function(){
    cy.get('ul li:nth-child(4)').should('be.visible').click();
    //cy.url().contains('include','@singhalok4it');
    cy.get('div:nth-child(1)  > div > div.pull-xs-right > button > i').should('be.visible').dblclick();
    cy.wait(10000);
    cy.get('a[href*="favorites"]').should('be.visible',{timeout:10000}).click();
    cy.wait(1000);
    cy.get('button.btn-primary').should('be.visible').first().then(($fav)=>{
     const favCount = $fav.text()
     expect(parseInt(favCount)).to.eq(1);
    });
    cy.get('div:nth-child(1)  > div > div.pull-xs-right > button > i').should('be.visible').click()
    cy.reload();
    cy.get('div.article-preview').should('be.visible').contains('No articles are here... yet.',{timeout:1000});
   })
})
