/// <reference types="cypress"/>
import LoginMethod from "../integration/PageObjectModel/PageClass/Login.Page.js";
import HomeMethod from "../integration/PageObjectModel/PageClass/Home.Page.js";

Cypress.Commands.add("Signin", () => {
    cy.fixture('TestData').then((data) => {
        LoginMethod.ClickOnSignInLink()
        LoginMethod.EnterEmail(data.Email)
        LoginMethod.EnterPassword(data.Password)
        LoginMethod.clickonSignInButton()
    })
    cy.wait(1000)
})

Cypress.Commands.add("Logout", () => {
    HomeMethod.clickOnSetting()
    HomeMethod.clickOnLogout()

})